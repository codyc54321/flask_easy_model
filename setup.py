from setuptools import setup


setup(name='flask-easy-model',
      description='A great base model, bringing Django conveniences to Flask',
      version='0.4.0',
      url='https://bitbucket.org/codyc54321/flask_easy_model',
      author='Cody Childers',
      author_email='cchilder@mail.usf.edu',
      license='MIT',
      classifiers=[
          'Programming Language :: Python :: 3'
      ],
      packages=['flask_easy_model'],
      install_requires=[ ],
      entry_points={ }
)
